import java.util.Scanner;

public class Billetes{
	public static void main(String args[]){
		int restante=0;
		int[] billetes={500, 200, 100, 50, 20, 10, 5};
		//System.out.println("Introduce la cantidad");
		//Scanner sc=new Scanner(System.in);
		//restante=sc.nextInt();
		restante=Integer.parseInt(args[0]);

		for(int i=0; i<billetes.length; i++){
			if(restante>=billetes[i]){
				System.out.println(restante/billetes[i]+ " billetes de "+billetes[i]);
				restante%=billetes[i];
			}
		}

		if(restante>0){
			System.out.println("Sobran "+restante+" euros");
		}
	}
}