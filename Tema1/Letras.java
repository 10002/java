public class Letras {
	public static char genAleatorio() {
		return (char)(26*Math.random()+65);
	}

	public static void main(String args[]){
		char letra=genAleatorio();
		if(letra=='A'||letra=='E'||letra=='I'||letra=='O'||letra=='U'){
			System.out.println(letra+" es una vocal");
		}else{
			System.out.println(letra+" es una consonante");		
		}
	}
}