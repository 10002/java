import java.util.Scanner;

public class Tiempo{
	public static void main(String args[]){
		int segundos=0, minutos=0, horas=0;
		//System.out.println("Introduce el tiempo en segundos");
		//Scanner sc=new Scanner(System.in);
		//segundos=sc.nextInt();

		segundos=Integer.parseInt(args[0]);

		minutos=segundos/60;
		segundos%=60;
		horas=minutos/60;
		minutos%=60;

		System.out.println(horas+" horas "+minutos+" minutos "+segundos+" segundos");
	}
}