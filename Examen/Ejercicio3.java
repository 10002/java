//Ejercicio 3. Eduardo Jiménez Bahíllo

public class Ejercicio3 {
	public static void main(String[] args) {
		int numero, digito, contador=0;

		numero=Integer.parseInt(args[0]);
		digito=Integer.parseInt(args[1]);

		while (numero>0) {
			if (numero%10 == digito) {
				contador++;
			}
			numero/=10;
		}

		System.out.println("El dígito "+digito+" se repite "+contador+" veces");
	}
}