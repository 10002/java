//Ejercicio 1. Eduardo Jiménez Bahíllo

import java.util.Scanner;

public class Ejercicio1 {
	public static void main(String[] args) {
		int opcion;
		double grados, grados2;
		Scanner sc=new Scanner(System.in);

		do {
			System.out.println("Elija una de las siguientes opciones:");
			System.out.println("	1) Pasar de Farenheit a Celsius");
			System.out.println("	2) Pasar de Celsius a Farenheit");
			System.out.println("	3) Salir");
			opcion=sc.nextInt();
			if (opcion==1) {
				System.out.println("Introduzca el número de grados Farenheit:");
				grados=sc.nextDouble();
				grados2=(grados-32)/1.8;
				System.out.println(grados+" grados Farenheit son "+grados2+" grados Celsius");
			} else if (opcion==2) {
				System.out.println("Introduzca el número de grados Celsius:");
				grados=sc.nextDouble();
				grados2=grados*1.8+32;
				System.out.println(grados+" grados Celsius son "+grados2+" grados Farenheit");
			}
		} while (opcion!=3);
	}
}