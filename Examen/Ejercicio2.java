//Ejercicio 2. Eduardo Jiménez Bahíllo

import java.util.Scanner;

public class Ejercicio2 {
	public static void main(String[] args) {
		int numero, suma=0, contador=0;
		double media;
		Scanner sc=new Scanner(System.in);

		System.out.println("Introduzca los números enteros positivos de los que quiere calcular la media (introduzca -1 para parar)");
		do {
			numero=sc.nextInt();
			if(numero!=-1) {
				suma+=numero;
				contador++;
			}
		} while (numero!=-1);

		media=(double)suma/contador;
		System.out.println("La media es: "+media);
	}
}