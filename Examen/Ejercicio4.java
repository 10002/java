//Ejercicio 4. Eduardo Jiménez Bahíllo

public class Ejercicio4 {
	public static int numAleatorio(int inf, int sup) {
        return (int) (sup*Math.random()+inf);
    }

    public static void imprimirPartidoN(int n) {
    	int random=numAleatorio(1,3);
    	if (n<10) {
    		System.out.print("Partido  "+n+": ");
    	} else {
    		System.out.print("Partido "+n+": ");
    	}

    	switch (random) {
    		case 1:
    			System.out.println("|1| | |");
    			break;
    		case 2:
    			System.out.println("| |X| |");
    			break;
    		case 3:
    			System.out.println("| | |2|");
    			break;
    	}
    }

	public static void main(String[] args) {
		for (int i=1; i<=15; i++) {
			imprimirPartidoN(i);
		}
	}
}