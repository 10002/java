//Ejercicio 5. Eduardo Jiménez Bahíllo

public class Ejercicio5 {
	public static void main(String[] args) {
		int filas;

		if (args.length!=1) {
			System.out.println("ERROR. Número de parámetros incorrecto. Introduce un número entero entre 1 y 9.");
			System.exit(1);
		}
		filas=Integer.parseInt(args[0]);
		if (filas<1 || filas>9) {
			System.out.println("ERROR. Debe introducir un número entero entre 1 y 9.");
			System.exit(2);
		}

		for (int i=1; i<=filas; i++) {
			for(int j=1; j<=filas-i; j++) {
				System.out.print(" ");
			}
			for(int j=1; j<=i; j++) {
				System.out.print(j);
			}
			for(int j=i-1; j>=1; j--) {
				System.out.print(j);
			}
			System.out.println("");
		}
	}
}