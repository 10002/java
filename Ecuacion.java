public class Ecuacion {
	public static void main(String args[]){
		double a = Double.parseDouble(args[0]);
		double b = Double.parseDouble(args[1]);
		double c = Double.parseDouble(args[2]);
		
		if (a==0&&b!=0){
			System.out.println(-c/b);
		} else if ((4*a*c) > Math.pow(b,2)){
			System.out.println("No tiene solución real");
		} else {
			System.out.println((-b+Math.sqrt(Math.pow(b,2)-4*a*c))/2*a);
			System.out.println((-b-Math.sqrt(Math.pow(b,2)-4*a*c))/2*a);
		}
	}
}