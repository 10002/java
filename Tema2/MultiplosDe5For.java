public class MultiplosDe5For{
	public static void main(String[] args){
		if(args.length!=1){
			System.out.println("ERROR número de parámetros incorrecto. Introduce un número entero.");
			System.exit(1);
		}
		int n=Integer.parseInt(args[0]);

		for (int i=1; i<=n; i++) {
			if(i%5==0){
				System.out.println(i);
			}
		}
	}
}