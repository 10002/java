public class Capicua2 {
	public static boolean esCapicua(int numero) {
		int invertido, restante;

		restante=numero;
		invertido=0; 

		while(restante>0) {
			invertido=invertido*10+restante%10;
			restante=restante/10;
		}

		if (numero==invertido) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {
		int numero=Integer.parseInt(args[0]);
		if (esCapicua(numero)) {
			System.out.println(numero+" es capicua");
		} else {
			System.out.println(numero+" no es capicua");
		}
	}
}