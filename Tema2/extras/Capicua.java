public class Capicua {
	public static int calcularDigitos(int n) {
		int digitos=1;
		while (n>9) {
			n/=10;
			digitos++;
		}
		return digitos;
	}

	public static void main(String[] args) {
		int num=Integer.parseInt(args[0]), digitos=calcularDigitos(num);
		boolean capicua=true;
		System.out.println(num+" tiene "+digitos+" digitos");
		/*for (int i=1; i<=digitos; i++) {
			System.out.println(num/(int)Math.pow(10,digitos-i)%10);
			System.out.println(num/(int)Math.pow(10,i-1)%10);
		}*/
		for (int i=1; i<=digitos/2 && capicua; i++) {
			if (num/(int)Math.pow(10,digitos-i)%10 != num/(int)Math.pow(10,i-1)%10) {
				capicua=false;
			}
		}
		if (capicua) {
			System.out.println(num+" es capicua");
		} else {
			System.out.println(num+" no es capicua");
		}
	}
}