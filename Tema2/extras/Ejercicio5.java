public class Ejercicio5 {
	public static int maximoComunDivisor(int a, int b) {
		if (b==0) {
			return a;
		} else {
			return maximoComunDivisor(b, a%b);
		}
	}

	public static int mcd(int a, int b) {
		int aux=0;
		while (b>0) {
			aux=a%b;
			a=b;
			b=aux;
		}
		return a;
	}

	public static int minimoComunMultiplo(int a, int b) {
		return a*b/maximoComunDivisor(a,b);
	}

	public static void main(String[] args) {
		int a=Integer.parseInt(args[0]), b=Integer.parseInt(args[1]);

		System.out.println("El máximo común divisor es "+maximoComunDivisor(a, b));
		System.out.println("El mínimo común múltiplo es "+minimoComunMultiplo(a, b));
	}
}