public class Ejercicio16 {
	public static void main(String[] args) {
		int n=0;
		if (args.length!=1) {
			System.out.println("ERROR. Número de parámetros incorrecto. Introduce un número entero impar mayor que 0.");
			System.exit(1);
		}
		try {
			n=Integer.parseInt(args[0]);
		} catch (Exception e) {
			System.out.println("ERROR. Tipo de parámetro incorrecto. Introduce un número entero impar mayor que 0.");
			System.exit(2);
		}
		if (n<1 || n%2==0) {
			System.out.println("ERROR. Tipo de parámetro incorrecto. Introduce un número entero impar mayor que 0.");
			System.exit(3);
		}

		for (int i=0; i<n/2+1; i++) {
			for (int j=0; j<(n/2-i); j++) {
				System.out.print(" ");
			}
			for (int j=0; j<2*i+1; j++) {
				System.out.print("*");
			}
			System.out.println("");
		}

		for (int i=0; i<n/2; i++) {
			for (int j=0; j<i+1; j++) {
				System.out.print(" ");
			}
			for (int j=0; j<n-(2*i+2); j++) {
				System.out.print("*");
			}
			System.out.println("");
		}

	}
}