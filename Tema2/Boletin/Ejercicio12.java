public class Ejercicio12 {
	public static int calcularCifras(int numero) {
		int digitos=1;
		while (numero>9) {
			numero/=10;
			digitos++;
		}
		return digitos;
	}

	public static boolean esArmstrong(int numero) {
		int suma=0, aux=numero, cifras=calcularCifras(numero);
		for (int i=1; i<=cifras; i++) {
			suma+=Math.pow(aux%10, cifras);
			aux/=10;
			//System.out.println(suma);
		}
		if (numero==suma) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {
		int n=0;
		if (args.length!=1) {
			System.out.println("ERROR. Número de parámetros incorrecto. Introduce un número entero positivo.");
			System.exit(1);
		}
		try {
			n=Integer.parseInt(args[0]);
		} catch (Exception e) {
			System.out.println("ERROR. Tipo de parámetro incorrecto. Introduce un número entero positivo.");
			System.exit(2);
		}
		if (n<0) {
			System.out.println("ERROR. Introduce un número entero positivo.");
			System.exit(3);
		}

		if (esArmstrong(n)) {
			System.out.println(n+" es Armstrong.");
		} else {
			System.out.println(n+ " no es Armstrong.");
		}
	}
}