public class Ejercicio15 {
	public static void main(String[] args) {
		int luckyNumber=0, aux=0;
		for(int i=0; i<10; i++) {
			if(i!=2 && i!=5) {
				System.out.println(i+" "+luckyNumber);
				luckyNumber+=Integer.parseInt(String.valueOf(args[0].charAt(i)));
			}
		}
		System.out.println(luckyNumber);
		
		while (luckyNumber>9) {
			aux=luckyNumber;
			luckyNumber=0;
			while (aux>0) {
				luckyNumber+=(aux%10);
				aux/=10;
			}
			System.out.println("while "+luckyNumber);
		}

		System.out.println("fin "+luckyNumber);
	}
}