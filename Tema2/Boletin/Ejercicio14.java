import java.util.Scanner;

public class Ejercicio14 {
	public static void main(String[] args) {
		String aux="";
		int numAleatorio=(int)(100*Math.random()+1), n=0;
		boolean acierto=false;
		//System.out.println(numAleatorio);

		for (int i=0; i<7 && !acierto; i++) {
			Scanner sc=new Scanner(System.in);
			System.out.println("Introduce un número entero entre 1 y 100: ");
			while (!sc.hasNextInt()) {
				aux=sc.nextLine();
				System.out.println(aux + " no es un número entero.");
				System.out.println("Introduce un número entero entre 1 y 100: ");
			}
			n=sc.nextInt();
			if (numAleatorio==n) {
				acierto=true;
			}
		}
		if (acierto) {
			System.out.println("Enhorabuena, has adivinado el número.");
		} else {
			System.out.println("Lo siento, no has adivinado el número.");
		}
	}
}