public class Ejercicio5 {
	public static void main(String[] args) {
		int limite=0, producto=1;
		if (args.length!=1) {
			System.out.println("Número de parámetros incorrecto");
			System.exit(1);
		}

		limite=Integer.parseInt(args[0]);

		if (limite<1) {
			System.out.println("Debe introducir un número entero positivo");
			System.exit(2);
		}

		for (int i=1; i<=limite; i++) {
			producto*=i;
		}
		System.out.println("El producto es "+producto);
	}
}