public class Ejercicio16v2 {
	public static String repite(String s, int n) {
		String salida="";
		for(int i=0; i<n; i++) {
			salida+=s;
		}
		return salida;
	}

	public static void main(String[] args) {
		int n=0, numEsp, numAst=1;
		if (args.length!=1) {
			System.out.println("ERROR. Número de parámetros incorrecto. Introduce un número entero impar mayor que 0.");
			System.exit(1);
		}
		try {
			n=Integer.parseInt(args[0]);
		} catch (Exception e) {
			System.out.println("ERROR. Tipo de parámetro incorrecto. Introduce un número entero impar mayor que 0.");
			System.exit(2);
		}
		if (n<1 || n%2==0) {
			System.out.println("ERROR. Tipo de parámetro incorrecto. Introduce un número entero impar mayor que 0.");
			System.exit(3);
		}

		numEsp=(n-1)/2;
		for (int fila=1; fila<=n; fila++) {
			System.out.println(repite("  ", numEsp)+repite("* ", numAst));
			if(fila<(n+1)/2) {
				numEsp--;
				numAst+=2;
			} else {
				numEsp++;
				numAst-=2;
			}
		}

		/*for (; numAst<=n; numAst+=2) {
			numEsp=n/2-numAst/2;
			System.out.print(repite(" ", numEsp));
			System.out.print(repite("*", numAst));
			System.out.println(numAst);
		}
		numAst-=4;
		for (; numAst>0; numAst-=2) {
			numEsp=n/2-numAst/2;
			System.out.print(repite(" ", numEsp));
			System.out.print(repite("*", numAst));
			System.out.println(numAst);
		}*/
	}
}