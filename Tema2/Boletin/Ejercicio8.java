public class Ejercicio8 {
	public static void main(String[] args) {
		double base=0, potencia=1;
		int exponente=0;
		boolean expNegativo=false;
		if (args.length!=2) {
			System.out.println("ERROR. Número de parámetros incorrecto. Introduce dos números enteros, la base y el exponente.");
			System.exit(1);
		}
		try {
			base=Double.parseDouble(args[0]);
			exponente=Integer.parseInt(args[1]);
		} catch (Exception e) {
			System.out.println("ERROR. Tipo de parámetro incorrecto. Introduce dos números, la base y el exponente entero.");
			System.exit(2);
		}

		if (exponente<0){
			exponente=-exponente;
			expNegativo=true;
		}
		for (int i=0; i<exponente; i++) {
			potencia*=base;
		}
		if (expNegativo) {
			potencia=1/potencia;
		}
		System.out.println(base+" elevado a "+exponente+" es "+potencia);
	}
}