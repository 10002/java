public class Ejercicio10 {
	public static void main(String[] args) {
		int n=0;
		String romano="";
		if (args.length!=1) {
			System.out.println("ERROR. Número de parámetros incorrecto. Introduce un número entero entre 1 y 3999.");
			System.exit(1);
		}
		try {
			n=Integer.parseInt(args[0]);
		} catch (Exception e) {
			System.out.println("ERROR. Tipo de parámetro incorrecto. Introduce un número entero entre 1 y 3999.");
			System.exit(2);
		}
		if (n>3999 || n<1) {
			System.out.println("ERROR. Introduce un número entero entre 1 y 3999.");
			System.exit(3);
		}

		while (n>=1000) {
			n-=1000;
			romano+="M";
		}
		while (n>=900) {
			n-=900;
			romano+="CM";
		}
		while (n>=500) {
			n-=500;
			romano+="D";
		}
		while (n>=400) {
			n-=400;
			romano+="CD";
		}
		while (n>=100) {
			n-=100;
			romano+="C";
		}
		while (n>=90) {
			n-=90;
			romano+="XC";
		}
		while (n>=50) {
			n-=50;
			romano+="L";
		}
		while (n>=40) {
			n-=40;
			romano+="XL";
		}
		while (n>=10) {
			n-=10;
			romano+="X";
		}
		while (n>=9) {
			n-=9;
			romano+="IX";
		}
		while (n>=5) {
			n-=5;
			romano+="V";
		}
		while (n>=4) {
			n-=4;
			romano+="IV";
		}
		while (n>=1) {
			n-=1;
			romano+="I";
		}
		//System.out.println(n);
		System.out.println(romano);
	}
}