public class Ejercicio6 {
	public static void main(String[] args) {
		int n=0;
		if (args.length!=1) {
			System.out.println("ERROR. Número de parámetros incorrecto. Introduce un número entero mayor que 2.");
			System.exit(1);
		}
		try {
			n=Integer.parseInt(args[0]);
		} catch (Exception e) {
			System.out.println("ERROR. Tipo de parámetro incorrecto. Introduce un número entero mayor que 2.");
			System.exit(2);
		}
		if(n<2){
			System.out.println("ERROR. Introduce un número entero mayor que 2.");
			System.exit(3);
		}

		for (int i=2; i<=n; i++) {
			for (int j=1; j<=10; j++) {
				System.out.println(i+"x"+j+"="+i*j);
			}
			System.out.println("-----------");
		}
	}
}