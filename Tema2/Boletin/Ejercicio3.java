import java.util.Scanner;

public class Ejercicio3 {
	static boolean comprobarParametros(String[] params) {
		if (params.length!=3) {
			System.out.println("Número de parámetros incorrecto.");
			return false;
		}
		try {
			Double.parseDouble(params[0]);
			Double.parseDouble(params[1]);
			Double.parseDouble(params[2]);
		} catch (Exception e) {
			System.out.println("Parámetros incorrectos. "+e.getMessage());
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		double a=0, b=0, c=0, discriminante=0, solucion1=0, solucion2=0;
		String aux="";

		if (comprobarParametros(args)) {
			a=Double.parseDouble(args[0]);
			b=Double.parseDouble(args[1]);
			c=Double.parseDouble(args[2]);
		} else {
			Scanner sc=new Scanner(System.in);
			System.out.println("Introduce el coeficiente cuadrático:");
			while (!sc.hasNextDouble()) {
				aux=sc.nextLine();
				System.out.println(aux + " no es un número.");
				System.out.println("Introduce el coeficiente cuadrático:");
			}
			a=sc.nextDouble();

			sc.nextLine();

			System.out.println("Introduce el coeficiente lineal:");
			while (!sc.hasNextDouble()) {
				aux=sc.nextLine();
				System.out.println(aux + " no es un número.");
				System.out.println("Introduce el coeficiente lineal:");
			}
			b=sc.nextDouble();

			sc.nextLine();

			System.out.println("Introduce el término independiente:");
			while (!sc.hasNextDouble()) {
				aux=sc.nextLine();
				System.out.println(aux + " no es un número.");
				System.out.println("Introduce el término independiente:");
			}
			c=sc.nextDouble();
			sc.close();
		}

		discriminante=Math.pow(b,2)-4*a*c;

		if (a==0) {
			System.out.println("No es una ecuación de segundo grado.");
		} else if (discriminante<0) {
			System.out.println("No tiene soluciones reales.");
		} else {
			solucion1=(-b+Math.sqrt(discriminante))/(2*a);
			solucion2=(-b-Math.sqrt(discriminante))/(2*a);
			System.out.println("Las soluciones son "+solucion1+" y "+solucion2);
		}
	}
}