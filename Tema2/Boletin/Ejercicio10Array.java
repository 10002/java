public class Ejercicio10Array {
	public static void main(String[] args) {
		int n=0;
		String romano="";
		int[] numeros={1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
		String[] letras={"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
		if (args.length!=1) {
			System.out.println("ERROR. Número de parámetros incorrecto. Introduce un número entero entre 1 y 3999.");
			System.exit(1);
		}
		try {
			n=Integer.parseInt(args[0]);
		} catch (Exception e) {
			System.out.println("ERROR. Tipo de parámetro incorrecto. Introduce un número entero entre 1 y 3999.");
			System.exit(2);
		}
		if (n>3999 || n<1) {
			System.out.println("ERROR. Introduce un número entero entre 1 y 3999.");
			System.exit(3);
		}

		for(int i=0; i<numeros.length; i++) {
			while (n>=numeros[i]) {
				n-=numeros[i];
				romano+=letras[i];
			}
		}
		
		//System.out.println(n);
		System.out.println(romano);
	}
}