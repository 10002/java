import java.util.Scanner;

public class Ejercicio9 {
	public static void main(String[] args) {
		int n=0, digitos=1;
		String aux="";
		Scanner sc=new Scanner(System.in);
		System.out.println("Introduce el número: ");
		while (!sc.hasNextInt()) {
			aux=sc.nextLine();
			System.out.println(aux + " no es un número entero");
			System.out.println("Introduce el primer número");
		}
		n=sc.nextInt();

		while (n>9) {
			n/=10;
			digitos++;
		}
		System.out.println("Tiene "+digitos+" dígitos");
	}
}