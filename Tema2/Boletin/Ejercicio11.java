public class Ejercicio11 {
	public static boolean esPrimo(int numero) {
		if (numero==1 || numero==2) {
			return true;
		}
		if (numero%2==0) {
			return false;
		}
		for (int i=3; i<=numero/2; i+=2) {
			if (numero%i==0) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		int n=0;
		if (args.length!=1) {
			System.out.println("ERROR. Número de parámetros incorrecto. Introduce un número entero.");
			System.exit(1);
		}
		try {
			n=Integer.parseInt(args[0]);
		} catch (Exception e) {
			System.out.println("ERROR. Tipo de parámetro incorrecto. Introduce un número entero.");
			System.exit(2);
		}
		if (n<0) {
			System.out.println("ERROR. Introduce un número entero positivo.");
			System.exit(3);
		}

		if (esPrimo(n)) {
			System.out.println(n+" es primo.");
		} else {
			System.out.println(n+" no es primo.");
		}
	}
}