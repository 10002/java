import java.util.Scanner;

public class Ejercicio1 {
	static boolean comprobarParametros(String[] params) {
		if (params.length!=3) {
			System.out.println("Número de parámetros incorrecto.");
			return false;
		}

		try {
			Integer.parseInt(params[0]);
			Integer.parseInt(params[1]);
			Integer.parseInt(params[2]);
		} catch (Exception e) {
			System.out.println("Parámetros incorrectos. "+e.getMessage());
			return false;
		}

		return true;
	}

	public static void main(String[] args) {
		int a, b, c, menor, mayor;
		String aux="";
		a=b=c=0;

		if (comprobarParametros(args)) {
			a=Integer.parseInt(args[0]);
			b=Integer.parseInt(args[1]);
			c=Integer.parseInt(args[2]);
		} else {
			Scanner sc=new Scanner(System.in);
			System.out.println("Introduce el primer número");
			while (!sc.hasNextInt()) {
				aux=sc.nextLine();
				System.out.println(aux + " no es un número entero");
				System.out.println("Introduce el primer número");
			}
			a=sc.nextInt();

			sc.nextLine();

			System.out.println("Introduce el segundo número");
			while (!sc.hasNextInt()) {
				aux=sc.nextLine();
				System.out.println(aux + " no es un número entero");
				System.out.println("Introduce el segundo número");
			}
			b=sc.nextInt();

			sc.nextLine();

			System.out.println("Introduce el tercer número");
			while (!sc.hasNextInt()) {
				aux=sc.nextLine();
				System.out.println(aux + " no es un número entero");
				System.out.println("Introduce el segundo número");
			}
			c=sc.nextInt();
			sc.close();
		}

		if (a<b && a<c) {
			menor=a;
		} else if (b<c) {
			menor=b;
		} else {
			menor=c;
		}
		if (a>b && a>c) {
			mayor=a;
		} else if (b>c) {
			mayor=b;
		} else {
			mayor=c;
		}
		System.out.println("El número menor es "+menor+" y el número mayor es "+mayor);
	}
}