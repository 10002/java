public class Nota{
	public static void main(String args[]){
		if(args.length!=1){
			System.out.println("Número de parámetros erróneo");
			System.exit(1);
		}
		switch(args[0].toUpperCase()){
			case "MATRICULA":
			case "MATRÍCULA": System.out.println("Has sacado un 10"); break;
			case "SOBRESALIENTE": System.out.println("Has sacado un 9"); break;
			case "NOTABLE ALTO": System.out.println("Has sacado un 8"); break;
			case "NOTABLE BAJO": System.out.println("Has sacado un 7"); break;
			case "BIEN": System.out.println("Has sacado un 6"); break;
			case "SUFICIENTE": System.out.println("Has sacado un 5"); break;
			case "SUSPENSO": System.out.println("Has sacado menos de un 5"); break;
			default: System.out.println("Nota no válida"); break;
		}
	}
}