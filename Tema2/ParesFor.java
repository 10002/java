public class ParesFor {
	public static void main(String[] args) {
		int n=0;
		if (args.length!=1) {
			System.out.println("ERROR. Número de parámetros incorrecto. Introduce un número entero.");
			System.exit(1);
		}
		try {
			n=Integer.parseInt(args[0]);
		} catch (Exception e) {
			System.out.println("ERROR. Tipo de parámetro incorrecto. Introduce un número entero.");
			System.exit(2);
		}

		for (int i=1; i<=n; i++) {
			if (i%2==0) {
				System.out.println(i);
			}
		}
	}
}