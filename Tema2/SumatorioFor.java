public class SumatorioFor{
	public static void main(String[] args) {
		int limite=Integer.parseInt(args[0]);
		int suma=0;

		for (int i=1; i<=limite; i++) {
			suma+=i;
		}

		System.out.println(suma);
	}
}