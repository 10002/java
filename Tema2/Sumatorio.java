public class Sumatorio{
	public static void main(String[] args) {
		int limite=Integer.parseInt(args[0]);
		int i=1;
		int suma=0;
		while(i<=limite){
			suma+=i;
			i++;
		}
		System.out.println(suma);
	}
}