public class Semana{
	public static void main(String[] args) {
		if(args.length!=1){
			System.out.println("Número de parámetros incorrectos");
			System.exit(1);
		}
		int dia=Integer.parseInt(args[0]);

		switch(dia){
			case 1: System.out.println("Lunes"); break;
			case 2: System.out.println("Martes"); break;
			case 3: System.out.println("Miércoles"); break;
			case 4: System.out.println("Jueves"); break;
			case 5: System.out.println("Viernes"); break;
			case 6: System.out.println("Sábado"); break;
			case 7: System.out.println("Domingo"); break;
			default: System.out.println("Debe introducir un número entre 1 y 7");
		}
	}
}