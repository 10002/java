public class TablaMultiplicar{
	public static void main(String[] args) {
		if(args.length!=1){
			System.out.println("ERROR número de parámetros incorrecto. Introduce un número entero.");
			System.exit(1);
		}
		int n=Integer.parseInt(args[0]);
		int i=1;
		while(i<=10) {
			System.out.println(n+"*"+i+"="+n*i);
			i++;
		}
	}
}