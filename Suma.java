import java.util.Scanner;

public class Suma{
    public static void main(String [] args){
		double n1=0, n2=0, suma=0;
		String a="";
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Introduce el primer número");
		while(!sc.hasNextDouble()){
			a=sc.nextLine();
			System.out.println(a + " no es un número");
			System.out.println("Introduce el primer número");
		}
		n1=sc.nextDouble();

		sc.nextLine();

		System.out.println("Introduce el segundo número");
		while(!sc.hasNextDouble()){
			a=sc.nextLine();
			System.out.println(a + " no es un número");
			System.out.println("Introduce el segundo número");
		}
		n2=sc.nextDouble();

		suma=n1+n2;
		System.out.println("LA SUMA ES: " + suma);
		sc.close();
    }
}