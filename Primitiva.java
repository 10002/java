public class Primitiva {
	public static byte genAleatorio(int inf, int sup) {
		return (byte)((sup-inf+1)*Math.random()+inf);
	}
	public static void main(String args[]){
		byte a=genAleatorio(1,49);
		byte b=genAleatorio(1,49);
		byte c=genAleatorio(1,49);
		byte d=genAleatorio(1,49);
		byte e=genAleatorio(1,49);
		byte f=genAleatorio(1,49);
		while (a==b) b=genAleatorio(1,49);
		while (a==c || b==c) c=genAleatorio(1,49);
		while (a==d || b==d || c==d) d=genAleatorio(1,49);
		while (a==e || b==e || c==e || d==e ) e=genAleatorio(1,49);
		while (a==f || b==f || c==f || d==f || e==f) f=genAleatorio(1,49);
		System.out.println(a + " " + b + " " + c + " " + d + " " + e + " " + f);
	}
}