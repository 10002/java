public class Joven {
	int edad;
	int nivelDeEstudios;
	double ingresos;
	boolean jasp;

	public Joven(int edad, int nivelDeEstudios, double ingresos){
		this.edad=edad;
		this.nivelDeEstudios=nivelDeEstudios;
		this.ingresos=ingresos;
	}

	public void establecerJasp(){
		jasp=(edad<=28&&nivelDeEstudios>3&&ingresos>28000);
	}

	public static void main(String args[]){
		Joven a=new Joven(18, 3, 1758.43);
		Joven b=new Joven(43, 5, 2258.43);
		a.establecerJasp();
		b.establecerJasp();
		System.out.println(a);
		System.out.println(b);
	}
}