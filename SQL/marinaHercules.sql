CREATE DATABASE IF NOT EXISTS marinaHercules;

CREATE TABLE IF NOT EXISTS marinaHercules.socio (
	DNI INT(8), 
	nombre VARCHAR(50) NOT NULL, 
	direccion VARCHAR(100) NOT NULL, 
	telefono VARCHAR(20) NOT NULL, 
	fechaIngreso DATETIME NOT NULL,
	PRIMARY KEY (DNI)
);

CREATE TABLE IF NOT EXISTS marinaHercules.embarcacion (
	matricula VARCHAR(30), 
	nombre VARCHAR(30) NOT NULL, 
	tipo VARCHAR(20) NOT NULL,
	dimensiones FLOAT(6,2) NOT NULL,
	propietario INT(8) NOT NULL,
	PRIMARY KEY (matricula),
	CONSTRAINT fk_embarcacion_socio FOREIGN KEY (propietario) REFERENCES socio(DNI)
);

CREATE TABLE IF NOT EXISTS marinaHercules.zona (
	letra CHAR(1),
	numBarcos INT(2) NOT NULL,
	tipoBarcos VARCHAR(30) NOT NULL, 
	profundidad FLOAT(4,2) NOT NULL, 
	ancho FLOAT(4,2) NOT NULL,
	PRIMARY KEY (letra)
);

CREATE TABLE IF NOT EXISTS marinaHercules.empleado (
	codEmpleado INT(3) AUTO_INCREMENT, 
	nombre VARCHAR(60) NOT NULL, 
	direccion VARCHAR(60) NOT NULL, 
	telefono VARCHAR(20) NOT NULL, 
	edad INT(2) NOT NULL,
	PRIMARY KEY (codEmpleado)
);

CREATE TABLE IF NOT EXISTS marinaHercules.zona_empleado (
	zona CHAR(1), 
	empleado INT(3), 
	barcosAsignados INT(2) NOT NULL,
	PRIMARY KEY (zona, empleado),
	CONSTRAINT fk_zona_empleado_zona FOREIGN KEY (zona) REFERENCES zona(letra),
	CONSTRAINT fk_zona_empleado_empleado FOREIGN KEY (empleado) REFERENCES empleado(codEmpleado)
);

CREATE TABLE IF NOT EXISTS marinaHercules.amarre (
    numAmarre CHAR(2),
	zona CHAR(1),
	agua FLOAT(10,2) NOT NULL,
	luz FLOAT(10,2) NOT NULL,
	servicioMantenimiento bool NOT NULL,
	arrendatario INT(8) NOT NULL,
	embarcacionOcupa VARCHAR(30) NOT NULL,
	fechaAlquiler DATETIME NOT NULL,
	fechaAsignacionBarco DATETIME NOT NULL,
	PRIMARY KEY (numAmarre, zona),
	CONSTRAINT fk_amarre_socio FOREIGN KEY (arrendatario) REFERENCES socio(DNI),
	CONSTRAINT fk_amarre_embarcacion FOREIGN KEY (embarcacionOcupa) REFERENCES embarcacion(matricula)
);