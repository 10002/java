public class Letras {
	public static char genAleatorio() {
		return (char)(26*Math.random()+65);
	}
	public static void main(String args[]){
		char a=genAleatorio();
		char b=genAleatorio();
		char c=genAleatorio();
		char d=genAleatorio();
		char e=genAleatorio();
		char f=genAleatorio();
		while (a==b) b=genAleatorio();
		while (a==c || b==c) c=genAleatorio();
		while (a==d || b==d || c==d) d=genAleatorio();
		while (a==e || b==e || c==e || d==e ) e=genAleatorio();
		while (a==f || b==f || c==f || d==f || e==f) f=genAleatorio();
		System.out.println(a + " " + b + " " + c + " " + d + " " + e + " " + f);
	}
}