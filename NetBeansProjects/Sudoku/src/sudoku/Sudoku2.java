/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudoku;

/**
 *
 * @author edu
 */
public class Sudoku2 {
    private int[][] cuadricula;
    
    public Sudoku2 () {
        cuadricula = new int[9][9];
    }
    
    public Sudoku2 (int[][] sudoku) {
        this();
        for (int i=0; i<9; i++) {
            for (int j=0; j<9; j++) {
                if (sudoku[i][j]>=1 && sudoku[i][j]<=9) {
                    cuadricula[i][j] = -sudoku[i][j];
                }
            }
        }
    }

    @Override
    public String toString() {
        String s = "+-------+-------+-------+\n";
        for (int i=0; i<9; i++) {
            s += "| ";
            for (int j=0; j<9; j++) {
                s += Math.abs(cuadricula[i][j]);
                if (j%3 == 2) {
                    s += " | ";
                } else {
                    s += " ";
                }
            }
            s += "\n";
            if (i%3 == 2) {
                s += "+-------+-------+-------+\n";
            }
        }
        return s;
    }
    
    public boolean resolver(int fila, int columna) {
        boolean sol = false;
        int fils, cols;
        fils = fila;
        cols = (columna+1)%9;
        if (cols == 0) {
            fils++;
        }
        
        if (fila == 9) {
            return true;
        }
        if (cuadricula[fila][columna]<0) {
            sol = resolver(fils, cols);
        } else {
            for (int i=1; i<=9 && !sol; i++) {
                if (comprobarCasilla(fila, columna, i)) {
                    cuadricula[fila][columna] = i;
                    sol = resolver(fils, cols);
                }
            }
        }
        if (!sol && cuadricula[fila][columna]>0) {
            cuadricula[fila][columna] = 0;
        }
        return sol;
    }
    
    public boolean comprobarCasilla(int fila, int columna, int n) {
        boolean b = true;
        for (int i=0; i<9 && b; i++) {
            if (Math.abs(cuadricula[fila][i])==n || Math.abs(cuadricula[i][columna])==n 
                    || Math.abs(cuadricula[i/3+fila/3*3][i%3+columna/3*3])==n) {
                b = false;
            }

        }
        return b;
    }
    
    public static void main(String [] args) {
        int[][] s = {   {0,0,0,0,0,0,0,0,0},
                        {0,0,3,0,0,0,7,0,0},
                        {0,2,0,1,6,0,0,8,0},
                        {0,0,0,0,3,0,5,0,0},
                        {7,0,6,0,0,0,0,2,0},
                        {4,0,0,0,0,0,1,6,0},
                        {0,0,0,0,0,1,0,4,0},
                        {0,5,0,9,0,0,0,0,6},
                        {9,0,0,4,0,0,0,0,8}};
        Sudoku2 sudoku = new Sudoku2(s);
        System.out.println(sudoku);
        sudoku.resolver(0,0);
        System.out.println(sudoku);
    }
}