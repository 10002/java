/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hashmaps;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author educe
 */
public class Secadores {
    private HashMap<Integer,String> nombres;
    private HashMap<Integer,Integer> ventas;
    
    public Secadores(String nombreFichero) {
        this.nombres = new HashMap<>();
        this.ventas = new HashMap<>();
        leerFichero(nombreFichero);
    }
    
    private void leerFichero(String nombreFichero) {
        try {
            Scanner fichero = new Scanner(new File(nombreFichero));
            while (fichero.hasNextLine()) {
                StringTokenizer st = new StringTokenizer(fichero.nextLine(), "\t");
                int numven = Integer.parseInt(st.nextToken());
                String nomven = st.nextToken();
                int monven = Integer.parseInt(st.nextToken());
                this.nombres.put(numven, nomven);
                if (this.ventas.containsKey(numven)) {
                    this.ventas.put(numven, this.ventas.get(numven)+monven);
                } else {
                    this.ventas.put(numven, monven);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Secadores.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(this.nombres);
        System.out.println(this.ventas);
    }
    
    public static void main(String[] args) {
        String f = "C:\\Users\\educe\\Documents\\secadores.txt";
        Secadores s = new Secadores(f);
    }
}
