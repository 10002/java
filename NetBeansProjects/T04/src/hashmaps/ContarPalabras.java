/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hashmaps;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author educe
 */
public class ContarPalabras {
    private final String separador;
    private String fichero;
    private HashMap<String,Integer> palabras;
    
    public ContarPalabras(String fichero) {
        this.fichero = fichero;
        this.separador = " ,.;\t\n\r";
        this.palabras = new HashMap<>();
    }
    
    public ContarPalabras(String fichero, String separador) {
        this.fichero = fichero;
        this.separador = separador;
        this.palabras = new HashMap<>();
    }
    
    public void contar() {
        try {
            Scanner sc = new Scanner(new File(fichero));
            while (sc.hasNextLine()) {
                StringTokenizer st = new StringTokenizer(sc.nextLine(), separador);
                while (st.hasMoreTokens()) {
                    String palabra = st.nextToken();
                    if (palabras.containsKey(palabra)) {
                        palabras.put(palabra, palabras.get(palabra)+1);
                    } else {
                        palabras.put(palabra, 1);
                    }
                }
            }
            System.out.println(palabras);
        } catch (FileNotFoundException e) {
            
        }
    }
    
    public static void main(String[] args) {
        String f = "C:\\Users\\educe\\Documents\\texto.txt";
        ContarPalabras cp = new ContarPalabras(f);
        cp.contar();
    }
}
