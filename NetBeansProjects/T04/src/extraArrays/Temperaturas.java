/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extraArrays;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author edu
 */
public class Temperaturas {
    private int[][] temp;
    private String[] mes = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
    
    public Temperaturas() {
        temp = new int[12][];
        for (int i=0; i<temp.length; i++) {
            switch (i) {
                case 3:
                case 5:
                case 8:
                case 10:
                    temp[i] = new int[30];
                    break;
                case 1:
                    temp[i] = new int[28];
                    break;
                default:
                    temp[i] = new int[31];
            }
        }
    }
    
    public Temperaturas(String file) {
        this();
        this.leerTemperaturas(file);
    }
    
    public void leerTemperaturas(String file) {
        try {
            FileInputStream is = new FileInputStream(new File(file));
            System.setIn(is);
            Scanner sc=new Scanner(System.in);
            for (int i=0; i<temp.length; i++) {
                for (int j=0; j<temp[i].length; j++){
                    temp[i][j] = sc.nextInt();
                }
            }
            sc.close();
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        }
    }
    
    @Override
    public String toString() {
        String s = "";
        for (int i=0; i<temp.length; i++) {
            s += "| ";
            for (int j=0; j<temp[i].length; j++) {
                s += temp[i][j] + " ";
            }
            s += "|\n";
        }
        return s;
    }
    
    public void mediaMes() {
        int sumatorio = 0;
        for (int i=0; i<temp.length; i++) {
            for (int j=0; j<temp[i].length; j++) {
                sumatorio += temp[i][j];
            }
            System.out.println(mes[i] + ": " + sumatorio/temp[i].length);
            sumatorio = 0;
        }
    }
    
    public void minMaxMes() {
        int min, max;
        for (int i=0; i<temp.length; i++) {
            min = max = 0;
            for (int j=1; j<temp[i].length; j++) {
                if (temp[i][j] < temp[i][min]) {
                    min = j;
                }
                if (temp[i][j] > temp[i][max]) {
                    max = j;
                }
            }
            System.out.println(mes[i] + ". Min: " + (min+1) + ". Max: " + (max+1));
        }
    }
    
    public void diaMasCaluroso() {
        int m = 0, d = 0;
        for (int i=0; i<temp.length; i++) {
            for (int j=1; j<temp[i].length; j++) {
                if (temp[i][j] > temp[m][d]) {
                    m = i;
                    d = j;
                }
            }
        }
        System.out.println("El día más caluroso del año fue el " + (d+1) + " de " + mes[m]);
    }
    
    public void diaMasFrio() {
        int m = 0, d = 0;
        for (int i=0; i<temp.length; i++) {
            for (int j=1; j<temp[i].length; j++) {
                if (temp[i][j] < temp[m][d]) {
                    m = i;
                    d = j;
                }
            }
        }
        System.out.println("El día más caluroso del año fue el " + (d+1) + " de " + mes[m]);
    }
    
    public static void main (String[] args) {
        Temperaturas t = new Temperaturas("C:\\Users\\educe\\Documents\\temperaturas.txt");
        System.out.println(t);
        t.mediaMes();
        t.minMaxMes();
        t.diaMasCaluroso();
        t.diaMasFrio();
    }
}
