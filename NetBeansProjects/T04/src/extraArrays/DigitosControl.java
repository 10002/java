/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extraArrays;

/**
 *
 * @author edu
 */
public class DigitosControl {
    public static String calcularDigitosControl(String entidad, String sucursal, String numCuenta) {
        String dc = "";
        int [] pesos = {1,2,4,8,5,10,9,7,3,6};
        int d1 = 0, d2 = 0;
        String entidadSucursal = "00"+entidad+sucursal;
        
        for (int i=0; i<10; i++) {
            d1 += ((entidadSucursal.charAt(i)-'0')%10)*pesos[i];
            d2 += ((numCuenta.charAt(i)-'0')%10)*pesos[i];
        }
        
        d1 = 11-(d1%11);
        d2 = 11-(d2%11);
        switch (d1) {
            case 10:
                dc = "1";
                break;
            case 11:
                dc = "0";
                break;
            default:
                dc += d1;
                break;
        }
       
        switch (d2) {
            case 10:
                dc += "1";
                break;
            case 11:
                dc += "0";
                break;
            default:
                dc += d2;
                break;
        }
        
        return dc;
    }
    
    public static int calcularDigitosControl(int entidad, int sucursal, long numCuenta) {
        int dc = 0;
        int [] pesos = {1,2,4,8,5,10,9,7,3,6};
        int aux = 0;

        for (int i=5; i>=2; i--) {
            aux += (entidad%10)*pesos[i];
            entidad /= 10;
        }
        
        for (int i=9; i>=5; i--) {
            aux += (sucursal%10)*pesos[i];
            sucursal /= 10;
        }
        
        aux = 11-(aux%11);
        if (aux==10) {
            dc = 10;
        } else if(aux==11) {
            dc = 0;
        } else {
            dc = aux*10;
        }
        
        aux = 0;
        
        for (int i=9; i>=0; i--) {
            aux += (numCuenta%10)*pesos[i];
            numCuenta /= 10;
        }
        
        aux = 11-(aux%11);
        if (aux==10) {
            dc += 1;
        } else if(aux==11) {
            dc += 0;
        } else {
            dc += aux;
        }
        
        return dc;
    }
    
    public static void main(String[] args) {
        System.out.println(calcularDigitosControl("1234","5678","0123456789"));
        System.out.println(calcularDigitosControl(1234,5678,123456789));
    }
}
