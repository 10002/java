/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extraArrays;

import java.util.Arrays;

/**
 *
 * @author edu
 */
public class CombinarArrays {
    public static int [] combinar(int[] array1, int[] array2) {
        int [] res = new int[array1.length+array2.length];
        int j=0, k=0;
        
        for (int i=0; i<res.length; i++) {
            if (k==array2.length || (j!=array1.length && array1[j]<=array2[k])) {
                res[i] = array1[j];
                j++;
            } else {
                res[i] = array2[k];
                k++;
            }
        }
        
        return res;
    }
    
    public static int[] mezclar(int[] array1, int[] array2) {
        int [] res = new int[array1.length+array2.length];
        int i, j;
        
        for (i=0, j=0; i<array1.length && j<array2.length;) {
            if (array1[i] <= array2[j]) {
                res[i+j] = array1[i];
                i++;
            } else {
                res[i+j] = array2[j];
                j++;
            }
        }
        while(i<array1.length) {
            res[i+j] = array1[i];
            i++;
        }
        while(j<array2.length) {
            res[i+j] = array2[j];
            j++;
        }
               
        return res;
    }
    
    public static void main (String [] args) {
        int [] a1 = {1,4,6,9,13,18,20,33,66,67,100};
        int [] a2 = {2,4,6,7,50,99};
        int [] a3 = mezclar(a2, a1);
        int [] a4 = combinar(a2,a3);
        
        System.out.println(Arrays.toString(a3));
        System.out.println(Arrays.toString(a4));
    }
}
