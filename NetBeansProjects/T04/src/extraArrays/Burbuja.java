/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extraArrays;

/**
 *
 * @author edu
 */
public class Burbuja {
    public static void Burbuja(int v[]) {
        int aux, i=0;
        boolean hayCambios=true;
        while (hayCambios) {
            hayCambios=false;
            for (int j=v.length-1; j>i; j--) {
                if (v[j-1]>v[j]) {
                    aux=v[j];
                    v[j]=v[j-1];
                    v[j-1]=aux;
                    hayCambios=true;
                }
            }
            i++;
        }
    }
    
    public static void main(String[] args) {
        int v[] = {7,3,4,55,72,12,9,66,11,10,90};
        Burbuja(v);
        for(int i=0; i<v.length; i++) {
            System.out.print(v[i] + " ");
        }
    }
}
