/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin;

/**
 *
 * @author daw
 */
public class Prueba {
    /* Método toUpper*/
    public static String convierteCadena(String s, boolean mayus) {
        String res=""; char c,inf,sup; int especiales[], inc;
        if (mayus) {
            especiales=new int[]{'á','é','í','ó','ú','ñ','ü'};
            inf='a'; sup='z'; inc='A'-'a';
        } else {
            especiales=new int[]{'Á','É','Í','Ó','Ú','Ñ','Ü'};
            inf='A'; sup='Z'; inc='a'-'A';
        }
        for(int i=0;i<s.length();i++) {
            c=s.charAt(i);
            if (inf <= c && c <= sup || UtilidadesMatrices.posicionArray(especiales,c)>=0 ) {
                c+=inc;
            }
            res+=c;
        }
        return res;
    }
    public static String toUpper(String s) {
        return convierteCadena(s,true);
    }
    public static String toLower(String s) {
        return convierteCadena(s,false);
    }

    public static void main(String [] args) {
        System.out.println(toUpper("AÁmdñfé"));
    }
}
