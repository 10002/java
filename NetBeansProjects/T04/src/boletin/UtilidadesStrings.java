/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin;

/**
 *
 * @author daw
 */
public class UtilidadesStrings {
    public static int cuentaRepetidos(char c, String s) {
        int contador = 0;
        
        for (int i=0; i<s.length(); i++) {
            if (s.charAt(i)==c) {
                contador++;
            }
        }
        
        return contador;
    }
    
    public static int cuentaRepetidos(String s1, String s2) {
        int contador = 0;
        
        for (int i=0; i<s2.length()-s1.length(); i++) {
            if (s1.equals(s2.substring(i, i+s1.length()))) {
                contador++;
            }
        }
        
        return contador;
    }
    
    public static int cuentaRepetidos2(String needle, String haystack) {
        int cont = 0;
        boolean encontrado;
        
        for (int i=0; i<=haystack.length()-needle.length();) {
            encontrado = true;
            for (int j=0; j<needle.length() && encontrado; j++) {
                //System.out.println(igual+"i="+i+" j="+j+" "+needle.charAt(j)+" "+haystack.charAt(i+j));
                if (needle.charAt(j)!=haystack.charAt(i+j)) {
                    encontrado = false;
                }
            }
            if (encontrado) {
                cont++;
                i+=needle.length();
            } else {
                i++;
            }
        }
        
        return cont;
    }
    
    public static String quitaEspacios(String s) {
        String res = "";
        for (int i=0; i<s.length(); i++) {
            if (s.charAt(i)!=' ') {
                res += s.charAt(i);
            }
        }
        return res;
    }
    
    public static String invertir(String s) {
        String res = "";
        
        for (int i=s.length()-1; i>=0; i--) {
            res += s.charAt(i);
        }
        
        return res;
    }
    
    public static boolean palindromo(String s) {
        return s.equals(invertir(s));
    }
    
    public static void ascii () {
        for (int c='a', C='A', n='0';c<='z';c++, C++, n++) {
            //System.out.println(i+"\t"+(char)i);
            if(n<='9') {
                System.out.printf("%3d %c\t%d %c\t%d %c\n",c,c,C,C,n,n);
            } else {
                System.out.printf("%d %c\t%d %c\t\n",c,c,C,C);
            }
        }
        /*for (int i='A';i<='Z';i++) {
            System.out.println(i+"\t"+(char)i);
        }
        for (int i='0';i<='9';i++) {
            System.out.println(i+"\t"+(char)i);
        }*/
        
    }
    
    public static void main(String [] args) {
        String s = "aaaaaaeijfoefjaavnlaa";
        String s2 = "aa";
        String s3 = "HolaHolaHolaHolaJuanHola";
        char c = 'a';
        
        //System.out.println(UtilidadesStrings.cuentaRepetidos2("Hola", s3));
        //System.out.println(UtilidadesStrings.quitaEspacios(s3));
        //System.out.println(UtilidadesStrings.invertir(s3));
        //System.out.println(UtilidadesStrings.palindromo(""));
        UtilidadesStrings.ascii();
    }
}
