/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin;

/**
 *
 * @author edu
 */
public class UtilidadesMatrices {
    public static int[] suma(int a[], int b[]) {
        if (a.length!=b.length || a.length==0) {
            return null;
        }
        
        int[] suma = new int[a.length];
        for (int i=0; i<suma.length; i++) {
            suma[i] = a[i] + b[i];
        }
        
        return suma;
    }
    
    public static int[][] suma(int a[][], int b[][]) {
        if (a.length!=b.length || a[0].length!=b[0].length) {
            return null;
        }
        
        int[][] suma = new int[a.length][a[0].length];
        for (int i=0; i<suma.length; i++) {
            for (int j=0; j<suma[0].length; j++) {
                suma[i][j] = a[i][j] + b[i][j];
            }
        }
        
        return suma;
    }
    
    public static int[] construyeArray(int n) {
        int [] v = new int[n];
        for (int i=0; i<n; i++) {
            v[i] = i+1;
        }
        return v;
    }
    
    public static int[] tablaMultiplicar(int n) {
        int t[] = new int[11];
        for (int i=0; i<t.length; i++) {
            t[i] = n*i;
        }
        return t; 
    }
    
    public static int maximo(int[] v) {
        int max=v[0];
        for(int i=1; i<v.length; i++) {
            if(max<v[i]) {
                max = v[i];
            }
        }
        return max;
    }
    
    public static int minimo(int[] v) {
        int min=v[0];
        for(int i=1; i<v.length; i++) {
            if(min>v[i]) {
                min = v[i];
            }
        }
        return min;
    }
    
    public static int[] minimoMaximo(int[] v) {
        int res[] = {v[0],v[0]};
        for(int i=1; i<v.length; i++) {
            if (v[0]>v[i]) {
                res[0] = v[i];
            }
            if (v[1]<v[i]) {
                res[1] = v[i];
            }
        }
        return res;
    }
    
    public static int posicionArray(int[] v, int n, int posIni) {
        if (posIni<0 || posIni>v.length-1) {
            return -1;
        }
        for (int i=posIni; i<v.length; i++) {
            if (v[i] == n) {
                return i;
            }
        }
        return -1;
    }
    
    public static int posicionArray(int[] v, int n) {
        return posicionArray(v, n, 0);
    }
    
    public static int[] posicionesArray(int[] v, int n) {
        int cont = 0;
        for (int i=0; i<v.length; i++) {
            if (v[i] == n) {
                cont++;
            }
        }
        int [] res = new int[cont];
        int pos=0;
        for (int i=0; pos<cont; i++) {
            if (v[i] == n) {
                res[pos] = i;
                pos++;
            }
        }
        return res;
    }
    
    public static int[] posicionesArray2(int[] a, int n) {
        int cont=0, pos, ind=0;
        for(int i=0;i<a.length;i++) {
            if (a[i]==n) {
                cont ++;
            }
        }
        System.out.println(UtilidadesMatrices.arrayToString(a));
        System.out.println("cont: "+cont);
        int[] res=new int[cont];
        for(pos=0;pos<a.length && pos!=-1;) { //for(;;);
            pos=posicionArray(a,n,pos);
            if (pos>=0) {
                res[ind]=pos;
                ind++;
                pos++;
                System.out.println("pos: "+pos);
            }
        }
        return res;
    }
    
    public static String arrayToString(int[] a) {
		String res="{"; int i;
		for(i=0;i<a.length;i++) {
			res+=a[i]+", ";
		}
		if (i<a.length) {
			res+=a[i];
		}
		res+="}";
		return res;
	}
    
    public static double mediaArray(int[] v) {
        double suma=0;
        for (int i=0; i<v.length; i++) {
            suma+=v[i];
        }
        return suma/v.length;
    }
    
    public static int[] invierteArray(int[] v) {
        int[] res = new int[v.length];
        for (int i=0; i<v.length; i++) {
            res[i] = v[v.length-1-i];
        }
        return res;
    }
    
    public static int numAleatorio(int inf, int sup) {
        return (int) (sup*Math.random()+inf);
    }
    
    public static int[] arrayAleatorio(int n, int inf, int sup) {
        int [] v = new int[n];
        int aux;
        for (int i=0; i<n; i++) {
            do {
                aux = numAleatorio(inf, sup);
            } while (posicionArray(v,aux)!=-1);
            v[i] = aux;
        }
        return v;
    }
    
    public static int[] construyeArray2(int n) {
        int [] v =  new int[n];
        if (n>0) {
            v[0] = 1;
            if (n>1) {
                v[1] = 1;
            }
        }
        for (int i=2; i<n; i++) {
            v[i] = v[i-1]*2;
        }
        return v;
    }

    public static boolean[] cribaEratostenes(int n) {
        boolean [] primos = new boolean[n+1];
        primos[1] = primos[2] = true;
        for (int i=3; i<primos.length; i++) {
            primos[i] = i%2 != 0;
        }
        
        int recorrido = (int)Math.sqrt(primos.length); 
        for (int i=3; i<recorrido; i+=2) {
            if (primos[i]) {
                for (int j=i*2; j<primos.length; j+=i) {
                    primos[j] = false;
                }
            }
        }
        return primos;
    }
    
    public static int cuentaRepetidos(int[] a) {
        int contador=0; boolean yaEsta;
        for (int i=0; i<a.length-1; i++) {
            yaEsta=false;
            for (int j=0; j<i; j++) {
                if (a[j]==a[i]) {
                    yaEsta=true;
                    break;
                }
            }
        
            if (!yaEsta) {
                for (int j=i+1;j<a.length;j++) {
                    if (a[i]==a[j]) {
                        contador++;
                    }
                }
            }
        }
        return contador;
    }
    /*
    public static int[] eliminarRepetidos (int[] a) {
        int[] res =new int[a.length-cuentaRepetidos(a)];
        for (int i=0, j=0; i<a.length; i++) {
            if (posicionArray(res,a[i]))
        }
    }*/
    
    public static int[] eliminaRepetidos(int[] v) {
        boolean aux[] = new boolean[v.length];
        for (int i=0; i<v.length; i++) {
            aux[i]=true;
        }
        
        for (int i=0; i<v.length; i++) {
            if (aux[i]) {
                for (int j=i+1; j<v.length; j++) {
                    if (v[i]==v[j]) {
                        aux[j]=false;
                    }
                }
            }
        }
        
        int cont=0;
        for (int i=0; i<aux.length; i++) {
            if (aux[i]) {
                cont++;
            }
        }

        int j=0;
        int[] res = new int[cont];
        for (int i=0; i<aux.length; i++) {
            if (aux[i]) {
                res[j]=v[i];
                j++;
            }   
        }
        return res;
        
        /*int res[] = new int[1];
        res[0] = v[0];
        for (int i=1; i<v.length; i++) {
            if (UtilidadesMatrices.posicionArray(res,v[i])==-1) {
                res = new int[res.length+1];
                res[res.length-1] = v[i];
            }
        }*/
    }
    
    public static void main(String[] args) {
        int[] pruebas={4,3,2,1,4,2,1,5,7,5,5};
        System.out.println(UtilidadesMatrices.cuentaRepetidos(pruebas));
        System.out.println(UtilidadesMatrices.arrayToString(UtilidadesMatrices.eliminaRepetidos(pruebas)));
        /*int[] posiciones=UtilidadesMatrices.posicionesArray2(pruebas,5); // {2,6,8}
        System.out.println(UtilidadesMatrices.arrayToString(posiciones));
        int[] v=UtilidadesMatrices.arrayAleatorio(6, 1, 49);
        System.out.println(UtilidadesMatrices.arrayToString(v));
        int[] v2=UtilidadesMatrices.construyeArray2(8);
        System.out.println(UtilidadesMatrices.arrayToString(v2));*/
    }
}