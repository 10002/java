/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin;

/**
 *
 * @author daw
 */
public class ExtraArrays {
    public static int [] combinar(int[] array1, int[] array2) {
        int [] res = new int[array1.length+array2.length];
        int j=0, k=0;
        
        for (int i=0; i<res.length; i++) {
            if (k==array2.length || (j!=array1.length && array1[j]<array2[k])) {
                res[i] = array1[j];
                j++;
            } else {
                res[i] = array2[k];
                k++;
            }
        }
        
        return res;
    }
    
    public static int calcularDigitosControl(int entidad, int sucursal, long numCuenta) {
        int dc = 0;
        int [] pesos = {1,2,4,8,5,10,9,7,3,6};
        int aux = 0;

        for (int i=5; i>=2; i--) {
            aux += (entidad%10)*pesos[i];
            entidad /= 10;
        }
        
        for (int i=9; i>=5; i--) {
            aux += (sucursal%10)*pesos[i];
            sucursal /= 10;
        }
        
        aux = 11-(aux%11);
        if (aux==10) {
            dc = 10;
        } else {
            dc = aux*10;
        }
        
        aux = 0;
        
        for (int i=9; i>=0; i--) {
            aux += (numCuenta%10)*pesos[i];
            numCuenta /= 10;
        }
        
        aux = 11-(aux%11);
        if (aux==10) {
            dc += 1;
        } else {
            dc += aux;
        }
        
        return dc;
    }
    
    public static void main (String [] args) {
        /*int [] a1 = {1,4,6,9,13,18,20,33,66,67,100};
        int [] a2 = {2,4,6,7,50,99};
        int [] a3 = combinar(a2, a1);
        
        for (int i=0; i<a3.length; i++) {
            System.out.print(a3[i]+" ");
        }*/
       
        System.out.println(calcularDigitosControl(1432,154,7422504551L));
        System.out.println(calcularDigitosControl(3243,5245,1341344635L));
    }
}
