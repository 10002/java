/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin;
import java.io.*;
import java.util.Scanner;

/**
 *
 * @author edu
 */
public class FabricaAutomoviles {
    //Cada fila será un centro de distribución, y cada columna un modelo de coche
    private int ventas[][];
    private final double[] PRECIOS = {1.5, 1.75, 2.42, 2.6};
    
    /**
     * Constructor por defecto. Inicializa las ventas a 0.
     */
    public FabricaAutomoviles() {
        ventas = new int[4][4];
    }
    
    /**
     *
     * @param ventas
     */
    public FabricaAutomoviles(int ventas[][]) {
        this();
        for (int i=0; i<4; i++) {
            for (int j=0; j<4; j++) {
                this.ventas[i][j] = ventas[i][j];
            }
        }
    }
    
    /**
     * Lee las ventas por teclado en pares centro modelo.
     */
    public void leerVentas() {
        int centro, modelo;
        Scanner sc=new Scanner(System.in);
        do {
            centro = sc.nextInt();
            if (centro!=-1) {
                modelo = sc.nextInt();
                ventas[centro][modelo]++;
            }
        } while(centro!=-1);
        sc.close();
    }
    
    /**
     * Lee las ventas a partir de un archivo en pares centro modelo.
     * @param file Archivo del que va a leer los datos
     */
    public void leerVentas(String file) {
        try {
            FileInputStream is = new FileInputStream(new File(file));
            System.setIn(is);
            leerVentas();
            System.setIn(System.in);
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * Calcula y muestra el volumen de ventas total. Apartado a.
     * @return Volumen de ventas total
     */
    public double volumenVentasTotal() {
        double total = 0;
        for (int i=0; i<4; i++) {
            for (int j=0; j<4; j++) {
                total += this.ventas[i][j]*PRECIOS[j];
            }
        }
        System.out.printf("Volumen de ventas total: %.2f millones\n",total);
        return total;
    }
    
    /**
     * Calcula y muestra el volumen de ventas por centro. Apartado b.
     * @return Volumen de ventas por centro
     */
    public double[] volumenVentasPorCentro() {
        double total[] = new double[4];
        for (int i=0; i<4; i++) {
            for (int j=0; j<4; j++) {
                total[i] += this.ventas[i][j]*PRECIOS[j];
            }
            System.out.printf("Volumen de ventas del centro %d: %.2f millones\n",i,total[i]);
        }
        return total;
    }
    
    /**
     *
     * @return
     */
    public int ventasTotal() {
        int total = 0;
        for (int i=0; i<4; i++) {
            for (int j=0; j<4; j++) {
                total += this.ventas[i][j];
            }
        }
        return total;
    }
    
    /**
     *
     * @return
     */
    public int[] ventasPorCentro() {
        int total[] = new int[4];
        for (int i=0; i<4; i++) {
            for (int j=0; j<4; j++) {
                total[i] += this.ventas[i][j]*PRECIOS[j];
            }
        }
        return total;
    }
    
    /**
     * Apartado c.
     * @return
     */
    public double[] porcentajeTotal() {
        int totalPorCentro[] = ventasPorCentro();
        int total = ventasTotal();
        double porcentajes[] = new double[4];
        for (int i=0; i<4; i++) {
            porcentajes[i] = (double)totalPorCentro[i]/total;
            System.out.printf("Porcentaje de unidades totales vendidas en el centro %d: %.2f%%\n",i,porcentajes[i]*100);
        }
        return porcentajes;
    }
    
    /**
     * Apartado d.
     * @return
     */
    public double[][] porcentajeModelos() {
        int total = ventasTotal();
        double porcentajes[][] = new double[4][4];
        for (int i=0; i<4; i++) {
            for (int j=0; j<4; j++) {
                porcentajes[i][j] = (double)this.ventas[i][j]/total;
                System.out.printf("Porcentaje de unidades del modelo %d en el centro %d: %5.2f%%\n",j,i,porcentajes[i][j]*100);
            }
        }
        return porcentajes;
    }
    
    /**
     *
     * @param args
     */
    public static void main(String [] args) {
        /*int ventas[][] =  {{10,90,10,10},
                             {20,20,20,20},
                             {25,25,25,25},
                             {50,50,50,50}};*/
        FabricaAutomoviles f = new FabricaAutomoviles();
        f.leerVentas("src/boletin/ventas.txt");
        System.out.println(f.ventasTotal());
        f.volumenVentasTotal();
        f.volumenVentasPorCentro();
        f.porcentajeTotal();
        f.porcentajeModelos();
    }
}