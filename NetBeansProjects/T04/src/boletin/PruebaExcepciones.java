/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin;

/**
 *
 * @author daw
 */
public class PruebaExcepciones {
    private int x;
    private int y;

    public PruebaExcepciones(int x, int y) {
        this.setY(y);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) throws Exception{
        if (x<0) {
            throw new Exception();
        }
        this.x = x;
    }

    @Override
    public String toString() {
        return "PruebaExcepciones{" + "x=" + x + ", y=" + y + '}';
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
}
