package boletin;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daw
 */
public class MatrizBid {
    private double[][] matriz;

    public MatrizBid() {
        this(1,1);
    }
    
    public MatrizBid(double[][] m) {
        this(m.length,m[0].length);
        for (int i=0; i<m.length; i++) {
            for (int j=0; j<m[0].length; j++) {
                this.matriz[i][j] = m[i][j];
            }
        }
    }

    public MatrizBid(int filas, int columnas) {
        matriz = new double[filas][columnas];
    }
    
    public MatrizBid(MatrizBid m) {
        this(m.getFilas(),m.getColumnas());
        for (int i=0; i<m.getFilas(); i++) {
            for (int j=0; j<m.getColumnas(); j++) {
                matriz[i][j] = m.matriz[i][j];
            }
        }
    }
    
    public int getFilas() {
        return this.matriz.length;
    }
    
    public int getColumnas() {
        return this.matriz[0].length;
    }
    
    public double getElemento(int fila, int columna) {
        return this.matriz[fila][columna];
    }

    @Override
    public String toString() {
        String s = "MatrizBid{";
        for (int i=0; i<getFilas(); i++) {
            s += "{";
            for (int j=0; j<getColumnas(); j++) {
                s += matriz[i][j];
                if (j!=matriz[0].length-1) {
                    s += ",";
                }
            }
            s += "}";
            if (i!=matriz.length-1) {
                s += ",";
            }
        }
        s += "}";
        return s;
    }
    
    public MatrizBid suma(MatrizBid m) {
        if (getFilas()!=m.getFilas() || getColumnas()!=m.getColumnas()) {
            System.out.println("ERROR. Las matrices deben tener el mismo tamaño.");
            return null;
        }
        double[][] res = new double[getFilas()][getColumnas()];
        for (int i=0; i<getFilas(); i++) {
            for (int j=0; j<getColumnas(); j++) {
                res[i][j] = this.matriz[i][j] + m.matriz[i][j];
            }
        }
        return new MatrizBid(res);
    }
    
    public MatrizBid resta(MatrizBid m) {
        if (getFilas()!=m.getFilas() || getColumnas()!=m.getColumnas()) {
            System.out.println("ERROR. Las matrices deben tener el mismo tamaño.");
            return null;
        }
        double[][] res = new double[getFilas()][getColumnas()];
        for (int i=0; i<getFilas(); i++) {
            for (int j=0; j<getColumnas(); j++) {
                res[i][j] = this.matriz[i][j] - m.matriz[i][j];
            }
        }
        return new MatrizBid(res);
    }
    
    public MatrizBid producto(MatrizBid m) {
        int f1=getFilas(), c1=getColumnas();
        int f2=m.getFilas(), c2=m.getColumnas();
        if (c1!=f2) {
            System.out.println("ERROR. El numero de columnas de la primera matriz debe ser igual al numero de filas de la segunda.");
            return null;
        }
        double[][] res = new double[f1][c2];
        for (int i=0; i<f1; i++) {
            for (int j=0; j<c2; j++) {
                for (int k=0; k<c1; k++) {
                    res[i][j] += this.matriz[i][k] * m.matriz[k][j];
                }
            }
        }
        return new MatrizBid(res);
    }
}
