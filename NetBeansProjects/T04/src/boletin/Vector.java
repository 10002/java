package boletin;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daw
 */
public class Vector {
    private double[] vector;

    public Vector() {
        
    }
    
    public Vector(double[] vector) {
        this.vector = new double[vector.length];
        for (int i=0; i<vector.length; i++) {
            this.vector[i] = vector[i];
        }
    }
    
    public Vector(Vector v) {
        this(v.vector);
    }
    
    public int getTamanio() {
        return vector.length;
    }
    
    public double getNumero(int posicion) {
        return vector[posicion];
    }
    
    public void setNumero(int posicion, double valor) {
        vector[posicion] = valor;
    }

    @Override
    public String toString() {
        String s = "Vector{vector=";
        for (int i=0; i<vector.length; i++) {
            if (i!=0) {
                s+=",";
            }
            s+=vector[i];
        }
        s+="}";
        return s;
    }
    
    public Vector suma(Vector v) {
        if (this.getTamanio()!=v.getTamanio()) {
            System.out.println("ERROR. Los vectores deben tener el mismo tamaño");
            return null;
        } else {
            int tam = this.getTamanio();
            double[] res = new double[tam];
            for (int i=0; i<tam; i++) {
                res[i] = this.getNumero(i)+v.getNumero(i);
            }
            return new Vector(res);
        }
    }
    
    public Vector resta(Vector v) {
        if (this.getTamanio()!=v.getTamanio()) {
            System.out.println("ERROR. Los vectores deben tener el mismo tamaño");
            return null;
        } else {
            int tam = this.getTamanio();
            double[] res = new double[tam];
            for (int i=0; i<tam; i++) {
                res[i] = this.getNumero(i)-v.getNumero(i);
            }
            return new Vector(res);
        }
    }
    
    public double productoEscalar(Vector v) {
        if (this.getTamanio()!=v.getTamanio()) {
            System.out.println("ERROR. Los vectores deben tener el mismo tamaño");
            return Double.NaN;
        } else {
            int tam = this.getTamanio();
            double res = 0;
            for (int i=0; i<tam; i++) {
                res+=this.getNumero(i)*v.getNumero(i);
            }
            return res;
        }
    }
    
    public double modulo() {
        double mod = 0;
        for (int i=0; i<vector.length; i++) {
            mod+=vector[i]*vector[i];
        }
        return Math.sqrt(mod);
    }
    
    public static void main(String[] args) {
        double[] a = {1,2,3,4,5};
        double[] b = {6,7,8,9,0};
        Vector v1 = new Vector(a);
        Vector v2 = new Vector(b);
        System.out.println(v1.suma(v2));
        System.out.println(v1.resta(v2));
        System.out.println(v1.productoEscalar(v2));
        System.out.println(v1.modulo());
    }
}