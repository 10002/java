/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daw
 */
import boletin.MatrizBid;
import boletin.UtilidadesMatrices;

public class Test {
    public static int[] suma(int[] a, int[] b) {
        int longitud, c[];
        if (a.length < b.length) {
            longitud = a.length;
        } else {
            longitud = b.length;
        }
        c = new int[longitud];
        for (int i=0; i<longitud; i++) {
            c[i] = a[i] + b[i];
        }
        return c;
    }
    
    public static int[][] suma(int[][] a, int[][] b) {
        int[][] c = new int[a.length][a[0].length];
        for (int i=0; i<a.length; i++) {
            for (int j=0; j<a[0].length; j++) {
                c[i][j] = a[i][j] + b[i][j];
            }
        }
        return c;
    }
    
    public static int[] construyeArray(int n) {
        int[] res = new int[n];
        for (int i=0; i<n; i++) {
            res[i] = i+1;
        }
        return res;
    }
    
    public static int[] construyeArray2(int n) {
        int[] res = new int[n];
        res[0] = 1;
        for (int i=1; i<n; i++) {
            //res[i] = 0;
            for (int j=0; j<i; j++) {
                res[i] = res[i] + res[j];
            }
        }
        return res;
    }
    
    public static void main(String[] args) {
        /*int[] a = {2,6,1,9,7,8,3,6,7,6,8,6};
        int[] b = {6,4,9,8,7,5,2};
        int[] c = UtilidadesMatrices.posicionesArray(a,6);
                
        for (int n: c) {
            System.out.print(n + " ");
        }*/
        boolean [] primos = UtilidadesMatrices.cribaEratostenes(100);
        for (int i=0; i<primos.length; i++) {
            if (primos[i]) {
                System.out.print(i + " ");
            }
        }
    }
    
    /*
    public static void main(String[] args) {
        int[] v = construyeArray2(7);
        for (int i=0; i<v.length; i++) {
            System.out.println(v[i] + " ");
        }
        
        
        int[][] a = {{1,2,3},{4,5,6},{7,8,9}};
        int[][] b = {{4,5,6},{7,8,9},{1,2,3}};
        int[][] c = suma(a,b);
        for (int[] c1 : c) {
            for (int c2 : c1) {
                System.out.print(c2 + " ");
            }
            System.out.println();
        }
        /*int[][] v = new int[2][];
        v[0] = new int[1];
        v[1] = new int[2];
        
        v[0][0] = 1;
        v[1][0] = 2;
        v[1][1] = 3;
        double m1[][] = {{1,2,3},{4,5,6}};
        MatrizBid mb1 = new MatrizBid(m1);
        double m2[][] = {{7,8,13},{9,10,14},{11,12,15}};
        MatrizBid mb2 = new MatrizBid(m2);
        MatrizBid suma = mb1.suma(mb2);
        MatrizBid producto1 = mb1.producto(mb2);
        //MatrizBid producto2 = mb2.producto(mb1);
        System.out.println(mb1);
        System.out.println(mb2);
        System.out.println(producto1);
        //System.out.println(producto2);
    }*/
    
    
}