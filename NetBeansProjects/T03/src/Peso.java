/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daw
 */
public class Peso {
    private double kg;
    public Peso(double peso, String medida) {
        switch (medida) {
            case "Lb":
                kg = peso*0.453;
                break;
            case "Li":
                kg = peso*14.59;
                break;
            case "Oz":
                kg = peso*0.02835;
                break;
            case "P":
                kg = peso*0.00155;
                break;
            case "K":
            	kg = peso;
            	break;
            case "G":
                kg = peso/1000;
                break;
            case "Q":
                kg = peso*43.3;
                break;
            default:
                kg = Double.NaN;
        }
    }
    public double getLibras() {
        return kg/0.453;
    }
    public double getLingotes() {
        return kg/14.59;
    }
    public double getPeso(String medida) {
    	switch (medida) {
            case "Lb":
                return kg/0.453;
            case "Li":
                return kg/14.59;
            case "Oz":
                return kg/0.02835;
            case "P":
                return kg/0.00155;
            case "K":
            	return kg;
            case "G":
                return kg*1000;
            case "Q":
                return kg/43.3;
            default:
                return kg;
        }
    }

    public static void main(String[] args) {
    	Peso p1, p2;

    	p1 = new Peso(2.2, "Lb");
    	System.out.println("2.2 libras son: ");
    	System.out.println("\t" + p1.getPeso("Lb") + " libras");
    	System.out.println("\t" + p1.getPeso("Li") + " lingotes");
    	System.out.println("\t" + p1.getPeso("Oz") + " onzas");
    	System.out.println("\t" + p1.getPeso("P") + " peniques");
    	System.out.println("\t" + p1.getPeso("K") + " kilogramos");
    	System.out.println("\t" + p1.getPeso("G") + " gramos");
    	System.out.println("\t" + p1.getPeso("Q") + " quintales");

    	p2 = new Peso(5, "K");
    	System.out.println("5 kilogramos son: ");
    	System.out.println("\t" + p2.getLibras() + " libras");
    	System.out.println("\t" + p2.getLingotes() + " lingotes");
    	System.out.println("\t" + p2.getPeso("Oz") + " onzas");
    	System.out.println("\t" + p2.getPeso("P") + " peniques");
    	System.out.println("\t" + p2.getPeso("K") + " kilogramos");
    	System.out.println("\t" + p2.getPeso("G") + " gramos");
    	System.out.println("\t" + p2.getPeso("Q") + " quintales");
    }
}
