/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daw
 */
public class Numero {
    private int numero;
    public Numero(){
        numero = 0;
    }
    public Numero(int numero) {
        this.numero = numero;
    }
    public void aniade(int numero) {
        this.numero+=numero;
    }
    public void resta(int numero) {
        this.numero-=numero;
    }
    public int getValor() {
        return numero;
    }
    public int getDoble() {
        return numero*2;
    }
    public int getTriple() {
        return numero*3;
    }
    public void setNumero(int numero) {
        this.numero = numero;
    }
}
