/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daw
 */
public class Consumo {
    private double kms, litros, vmed, pgas;
    public Consumo(double kms, double litros, double vmed, double pgas) {
        this.kms = kms;
        this.litros = litros;
        this.vmed = vmed;
        this.pgas = pgas;
    }
    public double getKms() {
        return kms;
    }
    public double getLitros() {
        return litros;
    }
    public double getVmed() {
        return vmed;
    }
    public double getPgas() {
        return pgas;
    }
    public void setKms(double kms) {
        this.kms = kms;
    }
    public void setLitros(double litros) {
        this.litros = litros;
    }
    public void setVmed(double vmed) {
        this.vmed = vmed;
    }
    public void setPgas(double pgas) {
        this.pgas = pgas;
    }
    public double getTiempo() {
        return kms/vmed;
    }
    public double consumoMedio() {
        return litros/kms*100;
    }
    public double consumoEuros() {
        return this.consumoMedio()*pgas;
    }
    @Override
    public String toString() {
        return "Consumo{" + "kms=" + kms + ", litros=" + litros + ", vmed=" + vmed + ", pgas=" + pgas + '}';
    }
    
}
