public class Coche {
    private String marca;
    private String modelo;
    public Coche() {
            this.marca = "Seat";
            this.modelo = "León";
    }
    public Coche(String marca, String modelo) {
            this.marca = marca;
            this.modelo = modelo;
    }
    public String getMarca() {
        return this.marca;
    }
    public String getModelo() {
        return this.modelo;
    }
    public void setMarca(String marca) {
        this.marca = marca;
    }
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    public String toString() {
        return "Marca: " + this.marca + ". Modelo: " + this.modelo;
    }
    public static void main(String[] args) {
        Coche c1 = new Coche(), c2 = new Coche("Mercedes", "Clase A");
        System.out.println(c1.toString());
        System.out.println(c2.toString());
    }
}