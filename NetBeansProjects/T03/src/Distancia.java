/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daw
 */
public class Distancia {
    public static double millasAMetrosQ(double millas) {
        return millas*1852;
    }
    public static double millasAKilometrosQ(double millas) {
        return millas*1.852;
    }

    public static void main(String[] args) {
    	System.out.println("28.13 millas son " + millasAMetrosQ(28.13) + " metros");
        System.out.println("7.92 millas son " + millasAKilometrosQ(7.92) + " kilometros");
    }
}