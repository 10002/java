/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daw
 */
public class MiNumero {
    public double numero;    
    public MiNumero(double numero) {
        this.numero = numero;
    }    
    public double doble(){
        return numero*2;
    }    
    public double triple(){
        return numero*3;
    }    
    public double cuadruple(){
        return numero*4;
    }    
    public static void main(String[] args) {
        MiNumero n = new MiNumero(2);
        System.out.println(n.doble());
        System.out.println(n.triple());
        System.out.println(n.cuadruple());
    }
}
