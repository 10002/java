/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daw
 */
public class VectorTridimensional {
    private double x, y, z;
    private static int numVectores=0;
    
    public VectorTridimensional() {
        this(0, 0, 0);
    }
    
    public VectorTridimensional(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
        VectorTridimensional.numVectores++;
    }
    
    public VectorTridimensional(VectorTridimensional v) {
        this(v.x, v.y, v.z);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public static int getNumVectores() {
        return numVectores;
    }

    @Override
    public String toString() {
        return "VectorTridimensional{" + "x=" + x + ", y=" + y + ", z=" + z + '}';
    }
    
    public VectorTridimensional suma(VectorTridimensional v) {
        return new VectorTridimensional(this.x+v.getX(), this.y+v.getY(), this.z+v.getZ());
    }
    
    public VectorTridimensional resta(VectorTridimensional v) {
        return new VectorTridimensional(this.x-v.getX(), this.y-v.getY(), this.z-v.getZ());
    }
    
    public double productoEscalar(VectorTridimensional v) {
        return this.x*v.getX()+this.y*v.getY()+this.z*v.getZ();
    }
    
    public VectorTridimensional productoVectorial(VectorTridimensional v) {
        double x = this.y*v.getZ()-v.getY()*this.z;
        double y = this.z*v.getX()-v.getZ()*this.x;
        double z = this.x*v.getY()-v.getX()*this.y;
        return new VectorTridimensional(x, y, z);
    }
    
    public double modulo() {
        return Math.sqrt(this.x*this.x+this.y*this.y+this.z*this.z);
    }
    
    public static VectorTridimensional vectorRaiz() {
        double r2 = Math.sqrt(2);
        return new VectorTridimensional(r2, r2, r2);
    }
}