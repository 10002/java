/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daw
 */
public class Restaurante {
    private double chocos, papas;
    public Restaurante() {
    	chocos = 0;
    	papas = 0;
    }
    public Restaurante(double chocos, double papas) {
    	this.chocos = chocos;
    	this.papas = papas;
    }
    public void addChocos(double x) {
    	chocos+=x;
    }
    public void addPapas(double x) {
    	papas+=x;
    }
    public int getComensales() {
        int n=0;
        while (chocos>=0.5 && papas>=1) {
            n+=3;
            chocos-=0.5;
            papas-=1.0;
        }
        return n;
    }
    public void showChocos() {
    	System.out.println("Hay " + chocos + " kg de chocos");
    }
    public void showPapas() {
    	System.out.println("Hay " + papas + " kg de papas");
    }
}
