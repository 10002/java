/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daw
 */
public class Finanzas {
    public double cambio;    
    public Finanzas() {
        cambio = 1.36;
    }    
    public Finanzas(double cambio) {
        this.cambio = cambio;
    }    
    public double dolaresToEuros(double dolares) {
        return dolares/cambio;
    }    
    public double eurosToDolares(double euros) {
        return euros*cambio;
    }    
    public static void main(String[] args) {
        Finanzas f = new Finanzas();
        System.out.println(f.dolaresToEuros(200));
        System.out.println(f.eurosToDolares(200));
    }
}
