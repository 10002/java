/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daw
 */
public class Complejo {
    private double a, b;
    
    public Complejo() {
        a = 0;
        b = 0;
    }
    
    public Complejo(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "Complejo{" + "a=" + a + ", b=" + b + '}';
    }
    
    public Complejo suma(Complejo c) {
        return new Complejo(this.a+c.getA(), this.b+c.getB());
    }
    
    public Complejo resta(Complejo c) {
        return new Complejo(this.a-c.getA(), this.b-c.getB());
    }
    
    public Complejo producto(Complejo com) {
        double a = this.a, b = this.b, c = com.getA(), d = com.getB();
        return new Complejo(a*c-b*d, a*d+b*c);
    }
    
    public Complejo division(Complejo com) {
        double a = this.a, b = this.b, c = com.getA(), d = com.getB();
        return new Complejo((a*c+b*d)/(c*c+d*d),(b*c-a*d)/(c*c+d*d));
    }
}
