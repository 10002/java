/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daw
 */
public class Empleado {
    private String DNI;
    private String nombre;
    private String apellidos;
    private double sueldoBruto;
    private double sueldoNeto;

    public Empleado() {}
    
    public Empleado(String DNI, String nombre, String apellidos, double sueldo) {
        setDNI(DNI);
        setNombre(nombre);
        setApellidos(apellidos);
        setSueldoBruto(sueldo);
    }
    
    public String getDNI() {
        return DNI;
    }

    public void setDNI(String DNI) {
        if (DNI.matches("[0-9]{8}[A-HJ-NP-TV-Z]")) {
            if (comprobarLetra(DNI)) {
                this.DNI = DNI;
            } else {
                System.out.println("La letra no coincide");
            }            
        } else {
            System.out.println("El texto introducido no es un DNI válido.");
        }        
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        if (nombre.length()<=30) {
            this.nombre = nombre;
        } else {
            System.out.println("El nombre debe tener menos de 30 caracteres");
        }
        
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        if (apellidos.length()<=80) {
            this.apellidos = apellidos;
        } else {
            System.out.println("Los apellidos deben tener menos de 80 caracteres");
        }
    }

    public double getSueldoBruto() {
        return sueldoBruto;
    }

    public void setSueldoBruto(double sueldoBruto) {
        this.sueldoBruto = sueldoBruto;
        this.sueldoNeto = sueldoBruto - sueldoBruto*0.15;
    }

    public double getSueldoNeto() {
        return sueldoNeto;
    }

    @Override
    public String toString() {
        return "Empleado{" + "DNI=" + DNI + ", nombre=" + nombre + ", apellidos=" + apellidos + ", sueldoBruto=" + sueldoBruto + ", sueldoNeto=" + sueldoNeto + '}';
    }
    
    public boolean comprobarLetra(String DNI) {
        char letra = DNI.charAt(DNI.length()-1);
        String letrasDNI = "TRWAGMYFPDXBNJZSQVHLCKE";
        int numeros = Integer.parseInt(DNI.substring(0, DNI.length()-1));
        return letra == letrasDNI.charAt(numeros%23);
    }
}
